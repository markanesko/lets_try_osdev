#!/bin/bash

export PREFIX="$HOME/opt/cross"
export TARGET=i686-elf
export PATH="$PREFIX/bin:$PATH"

cd src
rm *.o

nasm -felf32 asm/boot.s -o boot.o
nasm -felf32 asm/gdt_s.s -o gdt_s.o
nasm -felf32 asm/idt_s.s -o idt_s.o

i686-elf-gcc -c code/common.c -o common.o -std=gnu99 -ffreestanding -O2 -Wall -Wextra
i686-elf-gcc -c code/main.c -o main.o -std=gnu99 -ffreestanding -O2 -Wall -Wextra
i686-elf-gcc -c code/gdt.c -o gdt.o -std=gnu99 -ffreestanding -O2 -Wall -Wextra
i686-elf-gcc -c code/idt.c -o idt.o -std=gnu99 -ffreestanding -O2 -Wall -Wextra
i686-elf-gcc -c code/monitor.c -o monitor.o -std=gnu99 -ffreestanding -O2 -Wall -Wextra
i686-elf-gcc -c code/timer.c -o timer.o -std=gnu99 -ffreestanding -O2 -Wall -Wextra
i686-elf-gcc -c code/keyboard.c -o keyboard.o -std=gnu99 -ffreestanding -O2 -Wall -Wextra

i686-elf-gcc -T link.ld -o ../os.bin -ffreestanding -O2 -nostdlib boot.o gdt_s.o idt_s.o common.o main.o gdt.o idt.o monitor.o timer.o keyboard.o -lgcc

qemu-system-i386 -kernel ../os.bin
