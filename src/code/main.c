// main.c -- Defines the C-code kernel entry point, calls initialisation routines.

#include "multiboot.h"
#include "common.h"
#include "monitor.h"
#include "gdt.h"
#include "idt.h"
#include "timer.h"
#include "keyboard.h"

int kernel_main(multiboot_t *mboot_ptr)
{
  monitor_clear();

  monitor_write("Hello, world!\n");

  init_gdt ();
  init_idt ();
  asm volatile("int $0x3");

  //init_timer (20);

  asm volatile("sti");
    
  init_keyboard_driver();
  while(1)
  {
      char c = keyboard_getchar();
      if (c)
          monitor_put(c);
  }       


  return 0xfeecfeec;
}
